package com.example.kbhowal.library;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmhbh on 2/20/2018.
 */

public class CurrentlyCheckedOutRequest extends StringRequest{

    private static final String CHECKOUT_URL = "https://fblalibrary.000webhostapp.com/currently_checked_out.php";
    private Map<String, String> params;



    public CurrentlyCheckedOutRequest(String username, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Request.Method.POST, CHECKOUT_URL, listener, errorListener);

        params = new HashMap<>();
        params.put("Username", username);
    }

    //Override for getParams() method

    public Map<String, String> getParams(){

        return params;
    }
}

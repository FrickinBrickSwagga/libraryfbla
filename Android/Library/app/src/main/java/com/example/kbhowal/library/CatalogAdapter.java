package com.example.kbhowal.library;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import android.support.design.widget.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by kmhbh on 2/10/2018.
 */

public final class CatalogAdapter extends RecyclerView.Adapter<CatalogHolder> {

    Context context;
    List<Book> bookList;

    //Constructor

    public CatalogAdapter(Context context,List<Book> bookList){

        this.context = context;
        this.bookList = bookList;

    }

    //Creates a new view holder when there are no existing view holders which the RecyclerView can reuse

    @Override
    public CatalogHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.catalog_row_layout, null);

        CatalogHolder holder = new CatalogHolder(view);

        return holder;

    }

    //Binds data to view holder

    @Override
    public void onBindViewHolder(CatalogHolder holder, int position) {



        final Session session = new Session(context);

        final Book book = bookList.get(position);


        bookList.get(position).getBookId();


        //Binds Data
        loadImage(book.getCover(), holder.cover);

        int titleLength = book.getBookName().length();

        if(titleLength > 17){

            holder.title.setText(book.getBookName().substring(0, titleLength - 5) + "...");

        }
        else{

            holder.title.setText(book.getBookName());

        }

        holder.author.setText("By " + book.getAuthorName());


        //Clicking on the view holder will open the Master Detail view

        holder.setItemClickListener(new ItemClickListener() {
           @Override
           public void onItemClick(View view, int position) {

               Intent intent = new Intent(context, MasterDetailActivity.class);

               intent.putExtra("Id", book.getBookId());
               intent.putExtra("Title", book.getBookName());
               intent.putExtra("Author", book.getAuthorName());
               intent.putExtra("Pages", book.getBookPages());
               intent.putExtra("Language", book.getLanguage());
               intent.putExtra("Cover", book.getCover());
               intent.putExtra("Shortdesc", book.getShortdesc());



               context.startActivity(intent);
           }
        });

        //Clicking check out button will attempt to check the book out


        holder.checkOutButton.setBackgroundColor(0);
        holder.checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(!session.loggedIn()){

                    Snackbar.make(view, "You must log in to check out books. You may log in at the 'Account' tab.", Snackbar.LENGTH_LONG)
                            .show();
                }
                else{
                    CheckOutRequest checkOutRequest = new CheckOutRequest(book.getBookId(), session.sessionUser(), "", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            System.out.println(response);

                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                boolean success = jsonObject.getBoolean("success");
                                String message = jsonObject.getString("message");

                                if (success) {
                                    AlertDialog.Builder checkOutSuccess = new AlertDialog.Builder(context);
                                    checkOutSuccess.setMessage(message)
                                            .setNegativeButton("Ok", null)
                                            .create()
                                            .show();

                                } else {

                                    //If check out fails

                                    AlertDialog.Builder checkOutFail = new AlertDialog.Builder(context);
                                    checkOutFail.setMessage(message)
                                            .setNegativeButton("Ok", null)
                                            .setPositiveButton("Reserve a Copy", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    ReserveRequest reserveRequest = new ReserveRequest(book.getBookId(), session.sessionUser(), new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {

                                                            try {
                                                                JSONObject jsonObject = new JSONObject(response);


                                                                String message = jsonObject.getString("message");
                                                                boolean success = jsonObject.getBoolean("success");

                                                                if(success) {

                                                                    AlertDialog.Builder reserveSuccess = new AlertDialog.Builder(context);
                                                                    reserveSuccess.setMessage(message)
                                                                            .setNegativeButton("Ok", null)
                                                                            .create()
                                                                            .show();

                                                                }

                                                                else{

                                                                    //if reservation fails

                                                                    AlertDialog.Builder reserveFail = new AlertDialog.Builder(context);
                                                                    reserveFail.setMessage(message)
                                                                            .setNegativeButton("Ok", null)
                                                                            .create()
                                                                            .show();
                                                                }
                                                            } catch (JSONException e) {

                                                                e.printStackTrace();

                                                            }


                                                        }
                                                    });

                                                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                                                    requestQueue.add(reserveRequest);
                                                }
                                            })
                                            .create()
                                            .show();


                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, null);
                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    requestQueue.add(checkOutRequest);
                }

            }
        });

    }

    //Total books

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    //loads image from URL

    private void loadImage(String IMAGE_URL, ImageView View){

        Picasso.with(context).load(IMAGE_URL).placeholder(R.mipmap.ic_launcher)   //loads image from IMAGE_URL parameter (which will be a link)
                .error(R.mipmap.ic_launcher)    //The image that is displayed if the URL is not accessible
                .into(View, new Callback() {    //puts the image into the View specified by the parameter in the function. Then initiates a callback
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }
}

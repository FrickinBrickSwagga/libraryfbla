package com.example.kbhowal.library;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Method;

/**
 * Created by mbhowal on 12/24/2017.
 */

public class Book {

    //Initializes required variables

    public int bookId;
    public String uniqueId;
    public String bookName;
    public String authorName;
    public String bookPages;
    public String language;
    public String cover;
    public String shortdesc;
    public String coDate;
    public String dueDate;


    //Two possible constructors for calling the class


    public Book(int bookId, String uniqueId, String bookName, String authorName, String bookPages, String language, String cover, String shortdesc) {
        this.bookId = bookId;
        this.uniqueId = uniqueId;
        this.bookName = bookName;
        this.authorName = authorName;
        this.bookPages = bookPages;
        this.language = language;
        this.cover = cover;
        this.shortdesc = shortdesc;

    }



    public Book(int bookId, String uniqueId, String bookName, String authorName, String bookPages, String language, String cover, String shortdesc, String coDate, String dueDate){
        this.bookId = bookId;
        this.uniqueId = uniqueId;
        this.bookName = bookName;
        this.authorName = authorName;
        this.bookPages = bookPages;
        this.language = language;
        this.cover = cover;
        this.shortdesc = shortdesc;
        this.coDate = coDate;
        this.dueDate = dueDate;

    }

    //Getters and Setters

    public int getBookId() {
        return bookId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getBookName() {
        return bookName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getBookPages() {
        return bookPages;
    }

    public String getLanguage() {
        return language;
    }

    public String getShortdesc() {
        return shortdesc;
    }

    public String getCover() {
        return cover;
    }

    public String getCoDate() {
        return coDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public void setBookPages(String bookPages) {
        this.bookPages = bookPages;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public void setShortdesc(String shortdesc) {
        this.shortdesc = shortdesc;
    }

    public void setCoDate(String coDate) {
        this.coDate = coDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

}

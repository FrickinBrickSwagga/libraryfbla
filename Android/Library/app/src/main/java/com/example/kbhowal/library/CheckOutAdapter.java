package com.example.kbhowal.library;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import android.app.FragmentManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateMidnight;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by mbhowal on 12/25/2017.
 */

public class CheckOutAdapter extends RecyclerView.Adapter<CheckOutHolder> {


    Context context;
    List<Book> bookList;

    //Constructor

    public CheckOutAdapter(Context context, List<Book> bookList) {

        this.context = context;
        this.bookList = bookList;

    }

    //Creates a new view holder when there are no existing view holders which the RecyclerView can reuse

    @Override
    public CheckOutHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checked_out_row_layout, null);
        CheckOutHolder holder = new CheckOutHolder(view);

        return holder;

    }

    //Binds data to view holder

    @Override
    public void onBindViewHolder(CheckOutHolder holder, int position) {

        final Book book = bookList.get(position);
        final Session session = new Session(context);


        //Get due date and current date and finds the difference

        LocalDate dueDateTime = LocalDate.parse(parseDateToyyyyMMdd(book.getDueDate()));
        LocalDate currentDate = LocalDate.now();
        int days = Days.daysBetween(currentDate, dueDateTime).getDays();
        
        session.setHasOverdue(false);



        String dayWord = " days";

        //1 - 4 days are left

        if(days <= 4 && days > 1){
            holder.dueDate.setTextColor(Color.rgb(230, 200, 0));
            holder.daysLeft.setTextColor(Color.rgb(230, 200, 0));
        }

        //Less than 1 days is left

        else if (days <= 1){
            holder.dueDate.setTextColor(Color.RED);
            holder.daysLeft.setTextColor(Color.RED);

            dayWord = " day";


        }

        //More than 4 days are left

        else{
            holder.dueDate.setTextColor(Color.GREEN);
            holder.daysLeft.setTextColor(Color.GREEN);
        }

        //Setting data

        loadImage(book.getCover(), holder.cover);
        holder.title.setText(book.getBookName());
        holder.dueDate.setText("Due on " + book.getDueDate());
        holder.daysLeft.setText("(" + String.valueOf(days) + dayWord + ")");

        //If less than zero days are left

        if(days <= 0){
            holder.daysLeft.setText("Overdue");


            session.setHasOverdue(true);


        }

        //Clicking on the view holder will open the Master Detail view

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                Intent intent = new Intent(context, MasterDetailActivity.class);

                intent.putExtra("Id", book.getBookId());
                intent.putExtra("Title", book.getBookName());
                intent.putExtra("Author", book.getAuthorName());
                intent.putExtra("Pages", book.getBookPages());
                intent.putExtra("Language", book.getLanguage());
                intent.putExtra("Cover", book.getCover());
                intent.putExtra("Shortdesc", book.getShortdesc());


                context.startActivity(intent);
            }
        });

        //Clicking return button will attempt to return the book

        holder.returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {


                ReturnRequest returnRequest = new ReturnRequest(book.getUniqueId(), session.sessionUser(), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        AlertDialog.Builder logInAlert = new AlertDialog.Builder(context);
                        logInAlert.setMessage("Return Successful")
                                .setNegativeButton("Return Home", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        Intent intent = ((Activity)context).getIntent();
                                        ((Activity)context).finish();
                                        ((Activity)context).startActivity(intent);

                                    }
                                })
                                .create()
                                .show();

                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(returnRequest);



            }
        });


    }

    //Total books

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    //Loads image from URL

    private void loadImage(String IMAGE_URL, ImageView View){

        Picasso.with(context).load(IMAGE_URL).placeholder(R.mipmap.ic_launcher)   //loads image from IMAGE_URL parameter (which will be a link)
                .error(R.mipmap.ic_launcher)    //The image that is displayed if the URL is not accessible
                .into(View, new Callback() {    //puts the image into the View specified by the parameter in the function. Then initiates a callback
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    //Parses dates

    private String parseDateToyyyyMMdd(String time) {
        String outputPattern = "yyyy-MM-dd";
        String inputPattern = "MM-dd-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}

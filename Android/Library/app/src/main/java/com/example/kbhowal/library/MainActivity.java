package com.example.kbhowal.library;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.app.FragmentManager;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.io.Console;


public class    MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {



    FloatingActionMenu floatingActionMenu;
    FloatingActionButton floatingInsta, floatingFb;

    FragmentManager fragMan = getFragmentManager();

    //int id = item.getItemId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        floatingActionMenu = findViewById(R.id.social_floating_menu);

        floatingInsta = findViewById(R.id.floating_instagram);
        floatingFb = findViewById(R.id.floating_facebook);

        Session session = new Session(MainActivity.this);

        //Notification for id book is overdue

        if(session.hasOverDue()){

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setDefaults(NotificationCompat.DEFAULT_ALL)
                    .setSmallIcon(R.mipmap.library_icon)
                    .setContentTitle("Book(s) Overdue")
                    .setContentText("Please return your overdue book as soon as possible. Thank you.")
                    .setLargeIcon(Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.library_icon)));
            notificationBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);

            NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(MainActivity.this);
            notificationManagerCompat.notify(1, notificationBuilder.build());

            System.out.println("running");
        }


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");

        fragMan.beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView headerUsername = headerView.findViewById(R.id.headerUsername);

        //Settings nav_header text

        if(session.loggedIn()) {

            headerUsername.setText("Welcome " + session.sessionUser());

        }
        else{
            headerUsername.setText("Guest");

        }

        //Initializes the drawer layout

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(0).setChecked(true);

        floatingInsta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent instagramIntent = getOpenInstagramIntent(MainActivity.this);
                startActivity(instagramIntent);

            }
        });

        floatingFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent facebookIntent = getOpenFacebookIntent(MainActivity.this);
                startActivity(facebookIntent);
            }
        });


    }



    //On log in link click

    public void Here(View view){
        final TextView hereLink = (TextView) findViewById(R.id.LogInLink);

        hereLink.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Intent regIntent = new Intent(MainActivity.this, LogInActivity.class);
                MainActivity.this.startActivity(regIntent);

            }
        });
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Handles action bar item clicks The action bar will
        //automatically handle clicks on the Home/Up button
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handles navigation view item clicks

        getSupportActionBar().setTitle(item.getTitle());
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.rgb(48,63,159)));
        getSupportActionBar().show();
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            fragMan.beginTransaction().replace(R.id.content_frame, new HomeFragment()).commit();

        } else if (id == R.id.nav_catalog ) {

            fragMan.beginTransaction().replace(R.id.content_frame, new CatalogFragment()).commit();
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getSupportActionBar().setTitle("");



        } else if (id == R.id.nav_checked_out) {

            fragMan.beginTransaction().replace(R.id.content_frame, new CheckedOutFragment()).commit();

        } else if (id == R.id.nav_map) {

            fragMan.beginTransaction().replace(R.id.content_frame, new MapFragment()).commit();

        } else if (id == R.id.nav_contact) {

            fragMan.beginTransaction().replace(R.id.content_frame, new ContactFragment()).commit();

        } else if (id == R.id.nav_report) {

            fragMan.beginTransaction().replace(R.id.content_frame, new BugsFragment()).commit();

        } else if (id == R.id.nav_about) {

            fragMan.beginTransaction().replace(R.id.content_frame, new AboutFragment()).commit();

        } else if (id == R.id.nav_account) {

            fragMan.beginTransaction().replace(R.id.content_frame, new AccountFragment()).commit();

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;



    }



    public static Intent getOpenInstagramIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.instagram.android", 0); //Checks if Instagram is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/accounts/login/")); //Trys to make intent with Instagram's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.instagram.com/wrightacademylibrary/?hl=en")); //catches and opens a url to the desired page
        }
    }

    public static Intent getOpenFacebookIntent(Context context) {

        try {
            context.getPackageManager()
                    .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("fb://page/376227335860239")); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/Wrightacademylibrary-347538559066970/")); //catches and opens a url to the desired page
        }
    }

}



package com.example.kbhowal.library;

import android.media.MediaTimestamp;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmhbh on 1/1/2018.
 */

public class LogInRequest extends StringRequest {
    private static final String LOG_IN_URL = "https://fblalibrary.000webhostapp.com/login.php";
    private Map<String, String> params;


    //Data that will be sent to the database

    public LogInRequest(String username, String password, Response.Listener<String> listener){
        super(Method.POST, LOG_IN_URL, listener, null);
        params = new HashMap<>();

        params.put("Username", username);
        params.put("Password", password);

    }

    //Override for getParams() method

    public Map<String, String> getParams(){
        return params;
    }
}

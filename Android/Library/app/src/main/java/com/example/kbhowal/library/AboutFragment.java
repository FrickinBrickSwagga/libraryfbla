package com.example.kbhowal.library;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by kbhowal on 11/10/2017.
 */

public class AboutFragment extends Fragment {

    View view;      //Initializes the View

    @Nullable
    @Override

    //Sets content view

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.about_layout, container, false);
        return view;
    }
}

package com.example.kbhowal.library;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by kbhowal on 11/10/2017.
 */

public class ContactFragment extends Fragment {

    View view;      //Initializes the View

    //Sets content view

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_us_layout, container, false);
        return view;
    }
}

package com.example.kbhowal.library;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by kbhowal on 11/10/2017.
 */

public class BugsFragment extends Fragment {

    Button submitButton;

    //Sets content view

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.report_bugs_layout, container, false);


        submitButton = view.findViewById(R.id.submitButton);

        //On button click, the fragment will be refreshed and data will be submitted to database

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertDialog.Builder logInAlert = new AlertDialog.Builder(getActivity());
                logInAlert.setMessage("Your feedback has been submitted. Thank you for taking the time to help make the app better. You may supply more feedback if you wish.")
                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getFragmentManager().beginTransaction()
                                        .detach(BugsFragment.this)
                                        .attach(BugsFragment.this)
                                        .commit();
                            }
                        })
                        .create()
                        .show();
            }
        });


        return view;
    }
}

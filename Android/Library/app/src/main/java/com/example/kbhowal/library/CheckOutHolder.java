package com.example.kbhowal.library;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import javax.xml.transform.Templates;

/**
 * Created by mbhowal on 2/19/2018.
 */

public class CheckOutHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{

    ItemClickListener itemClickListener;

    //Initializing views
    ImageView cover;
    TextView title, dueDate, daysLeft;
    Button returnButton;

    public CheckOutHolder (View view){

        super (view);

        //Assigning views with a reference
        cover = view.findViewById(R.id.co_bookImage);
        title = view.findViewById(R.id.co_bookName);
        dueDate = view.findViewById(R.id.co_dueDate);
        daysLeft = view.findViewById(R.id.co_daysLeft);
        returnButton = view.findViewById(R.id.co_returnButton);

        view.setOnClickListener(this);
    }

    //On view holder click

    public void onClick(View view){

        this.itemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener itemClickListener){

        this.itemClickListener = itemClickListener;
    }

}

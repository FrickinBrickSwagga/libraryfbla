package com.example.kbhowal.library;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmhbh on 12/31/2017.
 */

public class RegisterRequest extends StringRequest {

    private static final String REGISTER_URL = "https://fblalibrary.000webhostapp.com/register.php";
    private Map<String, String> params;

    //Data that will be sent to the database

    public RegisterRequest(String username, String firstName, String lastName, String password, Response.Listener<String> listener){
        super(Method.POST, REGISTER_URL, listener, null);
        params = new HashMap<>();
        params.put("Username", username);
        params.put("FirstName", firstName);
        params.put("LastName", lastName);
        params.put("Password", password);
    }

    //Override for getParams() method

    public Map<String, String> getParams(){
        return params;
    }

}
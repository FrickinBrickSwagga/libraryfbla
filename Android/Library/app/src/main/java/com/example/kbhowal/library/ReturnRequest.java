package com.example.kbhowal.library;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mbhowal on 2/19/2018.
 */

public class ReturnRequest extends StringRequest {

    private static final String RETURN_URL = "https://fblalibrary.000webhostapp.com/return.php";
    private Map<String, String> params;

    //Data that will be sent to the database

    public ReturnRequest(String uniqueId, String username, Response.Listener<String> listener){
        super(Method.POST, RETURN_URL, listener, null);

        params = new HashMap<>();
        params.put("uniqueId", uniqueId);
        params.put("Username", username);
    }

    //Override for getParams() method

    public Map<String, String> getParams(){

        return params;
    }
}

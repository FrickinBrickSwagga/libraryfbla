package com.example.kbhowal.library;

import android.content.ClipData;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by kmhbh on 2/10/2018.
 */

public class CatalogHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    ItemClickListener itemClickListener;

    //Initializing Views

    ImageView cover;
    TextView title, author;
    Button checkOutButton;

    public CatalogHolder(View view){
        super(view);

        //Assigning views with a reference

        cover =  view.findViewById(R.id.bookImage);
        title = view.findViewById(R.id.bookName);
        author = view.findViewById(R.id.authorCat);
        checkOutButton = view.findViewById(R.id.checkOutButton);


        view.setOnClickListener(this);

    }

    //On view holder click

    @Override
    public void onClick(View view) {

        this.itemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener itemClickListener){

        this.itemClickListener = itemClickListener;

    }
}

package com.example.kbhowal.library;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by kbhowal on 11/10/2017.
 */

public class AccountFragment extends Fragment {

    View view;      //Initializes the View




    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        //Sets content view depending on whether a user is logged in

        final Session session = new Session(getActivity());
        if(session.loggedIn()) {
            view = inflater.inflate(R.layout.account_loggedin_layout, container, false);
            Button logOutButton = view.findViewById(R.id.logOutButton);

            //Log out button


            logOutButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    session.setLoggedIn(false);
                    AlertDialog.Builder logOut = new AlertDialog.Builder(getActivity());
                    logOut.setMessage("Logged out successfully")
                            .setNegativeButton("Return Home", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    getActivity().startActivity(intent);
                                }
                            })
                            .create()
                            .show();

                }
            });

            //Request for user first and last name from database

            CurrentlyCheckedOutRequest namesRequest = new CurrentlyCheckedOutRequest(session.sessionUser(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    JSONObject jsonObject = null;
                    try {

                        final TextView tvUser = view.findViewById(R.id.username);
                        final TextView tvFirstName = view.findViewById(R.id.first_name);
                        final TextView tvLastName = view.findViewById(R.id.last_name);

                        jsonObject = new JSONObject(response);
                        String firstName = jsonObject.getString("firstName");
                        String lastName = jsonObject.getString("lastName");

                        //Sets the data in the appropriate textView

                        tvUser.setText(session.sessionUser());
                        tvFirstName.setText(firstName);
                        tvLastName.setText(lastName);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                }
            }, null);

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(namesRequest);
        }

        //A different layout is loaded if user is not logged in

        else if(!session.loggedIn()){
            view = inflater.inflate(R.layout.account_layout, container, false);
        }

        return view;


    }

}







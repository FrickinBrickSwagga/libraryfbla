package com.example.kbhowal.library;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmhbh on 2/26/2018.
 */

public class ReserveRequest extends StringRequest {

    private static final String RESERVE_URL = "https://fblalibrary.000webhostapp.com/reserve.php";
    private Map<String, String> params;

    //Data that will be sent to the database

    public ReserveRequest(int bookId, String username, Response.Listener<String> listener){
        super(Request.Method.POST, RESERVE_URL, listener, null);

        params = new HashMap<>();
        params.put("bookId", String.valueOf(bookId));
        params.put("Username", username);
    }

    //Override for getParams() method

    public Map<String, String> getParams(){

        return params;
    }
}

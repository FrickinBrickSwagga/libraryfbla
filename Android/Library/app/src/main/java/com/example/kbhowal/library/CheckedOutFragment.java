package com.example.kbhowal.library;

import android.app.Activity;
import android.app.Fragment;
import android.database.CursorIndexOutOfBoundsException;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kbhowal on 11/10/2017.
 */

public class CheckedOutFragment extends Fragment {

    //Initializing lists and RecyclerViews


    List<Book> coBookList;
    List<Book> rBookList;
    RecyclerView coRecyclerView;
    RecyclerView rRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final Session session = new Session(getActivity());

        //Sets appropriate layout based on loggedIn status


        if(session.loggedIn()) {
            final View view = inflater.inflate(R.layout.checked_out_layout, container, false);        //Initializes the View

            coBookList = new ArrayList<>();
            rBookList = new ArrayList<>();

            //RecyclerView settings

            coRecyclerView = view.findViewById(R.id.rCheckedOutView);
            coRecyclerView.setHasFixedSize(true);
            coRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            rRecyclerView = view.findViewById(R.id.rReservedView);
            rRecyclerView.setHasFixedSize(true);
            rRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            fetchBooks();

            return view;

        }
        else{
            //other layout

            final View view = inflater.inflate(R.layout.loggedout_layout, container, false);
            return view;

        }
    }



    public void fetchBooks() {


        final Session session = new Session(getActivity());

        final CurrentlyCheckedOutRequest currentlyCheckedOutRequest = new CurrentlyCheckedOutRequest(session.sessionUser(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {


                        //Converting the string to JSONObject
                        JSONObject checkedOut = new JSONObject(response);


                        //Fetching all checkedOut data

                        String checkedOut1 = checkedOut.getString("CheckOut1");
                        String checkedOut2 = checkedOut.getString("CheckOut2");
                        String checkedOut3 = checkedOut.getString("CheckOut3");
                        String checkedOut4 = checkedOut.getString("CheckOut4");
                        String checkedOut5 = checkedOut.getString("CheckOut5");

                        //Defining data as strings

                        final String coDate1 = checkedOut.getString("coDate1");
                        final String coDate2 = checkedOut.getString("coDate2");
                        final String coDate3 = checkedOut.getString("coDate3");
                        final String coDate4 = checkedOut.getString("coDate4");
                        final String coDate5 = checkedOut.getString("coDate5");

                        final String dueDate1 = checkedOut.getString("dueDate1");
                        final String dueDate2 = checkedOut.getString("dueDate2");
                        final String dueDate3 = checkedOut.getString("dueDate3");
                        final String dueDate4 = checkedOut.getString("dueDate4");
                        final String dueDate5 = checkedOut.getString("dueDate5");

                        //Placing data in arrays

                        final String[] checkOutArray = new String[]{checkedOut1, checkedOut2, checkedOut3, checkedOut4, checkedOut5};
                        final String[] checkedOutDatesArray = new String[]{coDate1, coDate2, coDate3, coDate4, coDate5};
                        final String[] dueDatesArray = new String[]{dueDate1, dueDate2, dueDate3, dueDate4, dueDate5};

                        ///Traversing through the arrays

                        for (int i = 0; i < checkOutArray.length; i++) {

                            //Parsing dates

                            final String coDate = (parseDateToMMddyyyy(checkedOutDatesArray[i]));
                            final String dueDate = (parseDateToMMddyyyy(dueDatesArray[i]));

                            BookRequest bookRequest = new BookRequest(checkOutArray[i], new Response.Listener<String>() {
                                public void onResponse(String response) {
                                    try {

                                        //getting book object
                                        JSONObject bookData = new JSONObject(response);

                                        //adding the books to the appropriate bookList
                                        coBookList.add(new Book(
                                                bookData.getInt("bookId"),
                                                bookData.getString("uniqueId"),
                                                bookData.getString("bookName"),
                                                bookData.getString("authorName"),
                                                bookData.getString("bookPages"),
                                                bookData.getString("Language"),
                                                bookData.getString("cover"),
                                                bookData.getString("shortdesc"),
                                                coDate,
                                                dueDate));



                                        //creating adapter object and setting it to recyclerView
                                        CheckOutAdapter coAdapter = new CheckOutAdapter(getContext(), coBookList);
                                        coRecyclerView.setAdapter(coAdapter);




                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });



                            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                            requestQueue.add(bookRequest);
                        }




                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, null);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(currentlyCheckedOutRequest);

        CurrentlyCheckedOutRequest currentlyReservedRequest = new CurrentlyCheckedOutRequest(session.sessionUser(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {



                try {
                    //Converting the string to JSONObject

                    JSONObject jsonObject = new JSONObject(response);
                    final String reserved = jsonObject.getString("reserve1");

                    BookRequest bookRequest = new BookRequest(reserved, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {

                                //getting book object
                                JSONObject bookData = new JSONObject(response);

                                //adding the books to the appropriate bookList
                                rBookList.add(new Book(
                                        bookData.getInt("bookId"),
                                        bookData.getString("uniqueId"),
                                        bookData.getString("bookName"),
                                        bookData.getString("authorName"),
                                        bookData.getString("bookPages"),
                                        bookData.getString("Language"),
                                        bookData.getString("cover"),
                                        bookData.getString("shortdesc")));



                                //creating adapter object and setting it to recyclerView
                                ReserveAdapter rAdapter = new ReserveAdapter(getContext(), rBookList);
                                rRecyclerView.setAdapter(rAdapter);

                            } catch (JSONException e) {

                                e.printStackTrace();

                            }

                        }
                    });


                    RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                    requestQueue.add(bookRequest);


                } catch (JSONException e) {

                    e.printStackTrace();

                }






            }

        }, null);

        requestQueue.add(currentlyReservedRequest);


    }

    //Date parser

    private String parseDateToMMddyyyy(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "MM-dd-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }



}




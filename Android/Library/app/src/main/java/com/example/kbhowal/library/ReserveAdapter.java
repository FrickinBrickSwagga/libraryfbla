package com.example.kbhowal.library;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by kmhbh on 2/28/2018.
 */

public class ReserveAdapter extends RecyclerView.Adapter<ReserveHolder> {
    Context context;
    List<Book> bookList;

    //Constructor

    public ReserveAdapter(Context context, List<Book> bookList) {

        this.context = context;
        this.bookList = bookList;

    }

    //Creates a new view holder when there are no existing view holders which the RecyclerView can reuse

    @Override
    public ReserveHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reserve_row_layout, null);
        ReserveHolder holder = new ReserveHolder(view);

        return holder;

    }

    //Binds data to view holder

    @Override
    public void onBindViewHolder(final ReserveHolder holder, int position) {

        //Button is disabled

        holder.checkOutButton.setEnabled(false);


        final Book book = bookList.get(position);
        final Session session = new Session(context);

        //Sets appropriate data

        loadImage(book.getCover(), holder.cover);
        holder.title.setText(book.getBookName());

        //Clicking on the view holder will open the Master Detail view

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


                Intent intent = new Intent(context, MasterDetailActivity.class);

                intent.putExtra("Id", book.getBookId());
                intent.putExtra("Title", book.getBookName());
                intent.putExtra("Author", book.getAuthorName());
                intent.putExtra("Pages", book.getBookPages());
                intent.putExtra("Language", book.getLanguage());
                intent.putExtra("Cover", book.getCover());
                intent.putExtra("Shortdesc", book.getShortdesc());


                context.startActivity(intent);
            }
        });



        final ReserveDetailsRequest reserveDetailsRequest = new ReserveDetailsRequest(book.getBookId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                try {

                    //Converting the string to json object
                    JSONObject jsonObject = new JSONObject(response);

                    //Getting boolean   and a message from the jsonObject

                    boolean available = jsonObject.getBoolean("available");
                    String message = jsonObject.getString("message");

                    if(available){

                        //If the book is available, it will available for check out by the user who has it checked out

                        holder.checkOutButton.setEnabled(true);
                        holder.availability.setTextColor(Color.GREEN);
                    }
                    else{
                        holder.checkOutButton.setEnabled(false);
                        holder.availability.setTextColor(Color.RED);

                    }

                    holder.availability.setText(message);


                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(reserveDetailsRequest);

        holder.checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                CheckOutRequest checkOutRequest = new CheckOutRequest(book.getBookId(), session.sessionUser(), "Reserved", new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            //Converting the string to json object
                            JSONObject jsonObject = new JSONObject(response);

                            //Gets boolean and a message from the jsonObject
                            boolean success = jsonObject.getBoolean("success");
                            String message = jsonObject.getString("message");

                            if (success) {

                                //Check out success

                                AlertDialog.Builder checkOutSuccess = new AlertDialog.Builder(context);
                                checkOutSuccess.setMessage("Check Out Successful")
                                        .setNegativeButton("Return Home", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                                Intent intent = ((Activity)context).getIntent();
                                                ((Activity)context).finish();
                                                ((Activity)context).startActivity(intent);

                                            }
                                        })
                                        .create()
                                        .show();

                            } else {

                                //Check out fail

                                AlertDialog.Builder checkOutFail = new AlertDialog.Builder(context);
                                checkOutFail.setMessage(message)
                                        .setNegativeButton("Ok", null)
                                        .setPositiveButton("Reserve a Copy", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                ReserveRequest reserveRequest = new ReserveRequest(book.getBookId(), session.sessionUser(), new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {

                                                        try {
                                                            //Converting the string to json object
                                                            JSONObject jsonObject = new JSONObject(response);

                                                            //Getting boolean and a message from the jsonObject
                                                            String message = jsonObject.getString("message");
                                                            boolean success = jsonObject.getBoolean("success");

                                                            if(success) {

                                                                //Reservation success

                                                                AlertDialog.Builder reserveSuccess = new AlertDialog.Builder(context);
                                                                reserveSuccess.setMessage(message)
                                                                        .setNegativeButton("Ok", null)
                                                                        .create()
                                                                        .show();

                                                            }

                                                            else{

                                                                //Reservation fail

                                                                AlertDialog.Builder reserveFail = new AlertDialog.Builder(context);
                                                                reserveFail.setMessage(message)
                                                                        .setNegativeButton("Ok", null)
                                                                        .create()
                                                                        .show();
                                                            }
                                                        } catch (JSONException e) {

                                                            e.printStackTrace();

                                                        }


                                                    }
                                                });

                                                RequestQueue requestQueue = Volley.newRequestQueue(context);
                                                requestQueue.add(reserveRequest);
                                            }
                                        })
                                        .create()
                                        .show();


                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, null);
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(checkOutRequest);

            }
        });


    }

    //Total books

    @Override
    public int getItemCount() {
        return bookList.size();
    }

    //Loads image from URL

    private void loadImage(String IMAGE_URL, ImageView View){

        Picasso.with(context).load(IMAGE_URL).placeholder(R.mipmap.ic_launcher)   //loads image from IMAGE_URL parameter (which will be a link)
                .error(R.mipmap.ic_launcher)    //The image that is displayed if the URL is not accessible
                .into(View, new Callback() {    //puts the image into the View specified by the parameter in the function. Then initiates a callback
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }
}

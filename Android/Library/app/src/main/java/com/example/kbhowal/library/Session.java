package com.example.kbhowal.library;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by mbhowal on 12/27/2017.
 */

public class Session {
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    //Constructor

    public Session(Context context){
        this.context = context;

        //Initializing variables

        sharedPreferences = context.getSharedPreferences("user_session", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    //Methods stored in the sharedPreferences editor that can be called in any class

    public void setLoggedIn(boolean loggedIn){
        editor.putBoolean("loggedIn", loggedIn).commit();
    }

    public boolean loggedIn(){

        return sharedPreferences.getBoolean("loggedIn", false);
    }

    public void setSessionUser(String user){
        editor.putString("sessionUser", user).commit();
    }

    public String sessionUser(){

        return sharedPreferences.getString("sessionUser", "Guest");
    }

    public void setHasOverdue(boolean hasOverdue){
        editor.putBoolean("hasOverdue", hasOverdue).commit();
    }

    public boolean hasOverDue(){

        return sharedPreferences.getBoolean("hasOverdue", false);
    }
}

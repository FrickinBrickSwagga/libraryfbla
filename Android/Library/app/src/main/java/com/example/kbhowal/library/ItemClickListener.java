package com.example.kbhowal.library;

import android.view.View;

/**
 * Created by kmhbh on 2/15/2018.
 */

public interface ItemClickListener {

    //Method override

    void onItemClick(View view, int position);
}

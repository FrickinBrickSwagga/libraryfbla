package com.example.kbhowal.library;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by kmhbh on 2/15/2018.
 */

public class MasterDetailActivity extends AppCompatActivity {

    //Initializing views

    ImageView coverMd;
    TextView titleMd, authorMd, languageMd, pagesMd, shortDescMd;

    Button backButton;
    Button checkOutButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.master_detail_layout);

        final Session session = new Session(this);
        //Get data from intent

        Intent intent = getIntent();

        //Getting all data from view holders

        final int bookId = intent.getExtras().getInt("Id");
        final String title = intent.getExtras().getString("Title");
        final String author = intent.getExtras().getString("Author");
        final String pages = intent.getExtras().getString("Pages");
        final String language = intent.getExtras().getString("Language");
        final String cover = intent.getExtras().getString("Cover");
        final String shortDesc = intent.getExtras().getString("Shortdesc");

        //Reference views from xml
        titleMd = findViewById(R.id.md_title);
        authorMd = findViewById(R.id.md_author);
        pagesMd = findViewById(R.id.md_pages);
        languageMd = findViewById(R.id.md_language);
        coverMd = findViewById(R.id.md_bookImage);
        shortDescMd = findViewById(R.id.md_shortDesc);

        //Sets data
        titleMd.setText(title);
        authorMd.setText("Author: " + author);
        pagesMd.setText("Pages: " + pages);
        languageMd.setText("Language: " + language);
        loadImage(cover, coverMd);
        shortDescMd.setText(Html.fromHtml("<b>" + "Description: " + "</b>" + shortDesc));

        //Function for back button
        backButton = findViewById(R.id.md_backButton);
        checkOutButton = findViewById(R.id.md_checkOutButton);

        //Back button stops activity

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        checkOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Must be logged in to check out books

                if (!session.loggedIn()) {

                    Snackbar.make(view, "You must log in to check out books. You may log in at the 'Account' tab.", Snackbar.LENGTH_LONG)
                            .show();
                }



                else {
                    CheckOutRequest checkOutRequest = new CheckOutRequest(bookId, session.sessionUser(), "", new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {


                                //Converting the string to json object
                                JSONObject jsonObject = new JSONObject(response);

                                //Getting boolean and a message from the jsonObject
                                boolean success = jsonObject.getBoolean("success");
                                String message = jsonObject.getString("message");

                                if (success) {

                                    //Check out success

                                    AlertDialog.Builder checkOutSuccess = new AlertDialog.Builder(MasterDetailActivity.this);
                                    checkOutSuccess.setMessage(message)
                                            .setNegativeButton("Ok", null)
                                            .create()
                                            .show();

                                } else {

                                    //Check out fail

                                    AlertDialog.Builder checkOutFail = new AlertDialog.Builder(MasterDetailActivity.this);
                                    checkOutFail.setMessage(message)
                                            .setNegativeButton("Ok", null)
                                            .setPositiveButton("Reserve a Copy", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    ReserveRequest reserveRequest = new ReserveRequest(bookId, session.sessionUser(), new Response.Listener<String>() {
                                                        @Override
                                                        public void onResponse(String response) {

                                                            try {

                                                                //Converting the string to json array object
                                                                JSONObject jsonObject = new JSONObject(response);

                                                                //Getting boolean and a message from the jsonObject
                                                                String message = jsonObject.getString("message");
                                                                boolean success = jsonObject.getBoolean("success");

                                                                if(success) {

                                                                    //Reservation success

                                                                    AlertDialog.Builder reserveSuccess = new AlertDialog.Builder(MasterDetailActivity.this);
                                                                    reserveSuccess.setMessage(message)
                                                                            .setNegativeButton("Ok", null)
                                                                            .create()
                                                                            .show();

                                                                }

                                                                else{

                                                                    //Reservation fail

                                                                    AlertDialog.Builder reserveFail = new AlertDialog.Builder(MasterDetailActivity.this);
                                                                    reserveFail.setMessage(message)
                                                                            .setNegativeButton("Ok", null)
                                                                            .create()
                                                                            .show();
                                                                }
                                                            } catch (JSONException e) {

                                                                e.printStackTrace();

                                                            }


                                                        }
                                                    });

                                                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                                                    requestQueue.add(reserveRequest);
                                                }
                                            })
                                            .create()
                                            .show();


                                }



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, null);
                    RequestQueue requestQueue = Volley.newRequestQueue(MasterDetailActivity.this);
                    requestQueue.add(checkOutRequest);
                }
            }

            ;


        });

    }

    //Loads image from URL

    private void loadImage(String IMAGE_URL, ImageView View){

        Picasso.with(this).load(IMAGE_URL).placeholder(R.mipmap.ic_launcher)   //loads image from IMAGE_URL parameter (which will be a link)
                .error(R.mipmap.ic_launcher)    //The image that is displayed if the URL is not accessible
                .into(View, new Callback() {    //puts the image into the View specified by the parameter in the function. Then initiates a callback
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }
}

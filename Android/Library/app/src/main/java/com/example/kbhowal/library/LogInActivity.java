package com.example.kbhowal.library;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.security.auth.login.LoginException;


/**
 * Created by kbhowal on 11/10/2017.
 */


public class LogInActivity extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        final Session session = new Session(this);

        //Assigning variables to layout textViews

        final EditText logUser = (EditText) findViewById(R.id.LogUser);
        final EditText logPass = (EditText) findViewById(R.id.LogPass);
        final Button logInButton = (Button) findViewById(R.id.LogInButton);
        final Button cancelButton = (Button) findViewById(R.id.CancelButton);
        final TextView regLink = (TextView) findViewById(R.id.RegLink);

        //Cancel button stops the activity

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }

        });

        //Opens register activity

        regLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent regIntent = new Intent(LogInActivity.this, RegisterActivity.class);
                LogInActivity.this.startActivity(regIntent);

            }
        });

        //When the log in button is clicked

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Gets the user inputted text

                final String username = logUser.getText().toString();
                String password = logPass.getText().toString();

                //Response listener

                Response.Listener<String> listener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            //Converting the string to json object
                            JSONObject jsonObject = new JSONObject(response);

                            //Getting boolean from the object
                            boolean success = jsonObject.getBoolean("success");

                            if(success){

                                //Log in success

                                AlertDialog.Builder logInAlert = new AlertDialog.Builder(LogInActivity.this);
                                logInAlert.setMessage("Log In Successful")
                                        .setNegativeButton("Return Home", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                                                LogInActivity.this.startActivity(intent);
                                                finish();
                                            }
                                        })
                                        .create()
                                        .show();

                                //Sets the session user and sets the loggedIn() boolean to true

                                session.setLoggedIn(true);
                                session.setSessionUser(username);


                            }
                            else{

                                //Log In fail

                                AlertDialog.Builder logInFailure = new AlertDialog.Builder(LogInActivity.this);
                                logInFailure.setMessage("Incorrect Username/Password")
                                        .setNegativeButton("Try Again", null)
                                        .create()
                                        .show();

                            }



                        }
                        catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                };

                //Sends the user inputted data and sends it to the server for verification

                LogInRequest logInRequest = new LogInRequest(username, password, listener);
                RequestQueue requestQueue = Volley.newRequestQueue(LogInActivity.this);
                requestQueue.add(logInRequest);

            }
        });
    }
}



package com.example.kbhowal.library;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by kmhbh on 2/28/2018.
 */

public class ReserveHolder extends RecyclerView.ViewHolder implements  View.OnClickListener{

    ItemClickListener itemClickListener;

    //Initializing views
    ImageView cover;
    TextView title, availability;
    Button checkOutButton;

    public ReserveHolder (View view){
        super (view);

        //Assigning views with a reference
        cover = view.findViewById(R.id.r_bookImage);
        title = view.findViewById(R.id.r_bookName);
        availability = view.findViewById(R.id.r_availability);

        checkOutButton = view.findViewById(R.id.r_checkOutButton);

        view.setOnClickListener(this);
    }

    //On view holder click

    public void onClick(View view){

        this.itemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(ItemClickListener itemClickListener){

        this.itemClickListener = itemClickListener;
    }
}

package com.example.kbhowal.library;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity{

    public void onCreate(Bundle savedInstance)
    {
        super.onCreate(savedInstance);
        setContentView(R.layout.register_layout);

        //Assigning variables to each of the 'EditText's and Buttons

        final EditText edFirstName = (EditText) findViewById(R.id.FirstName);
        final EditText edLastName = (EditText) findViewById(R.id.LastName);
        final EditText edRegUser = (EditText) findViewById(R.id.RegUser);
        final EditText edRegPass = (EditText) findViewById(R.id.RegPass);
        final EditText edConfPass = (EditText) findViewById(R.id.ConfPass);
        final Button regButton = (Button) findViewById(R.id.RegisterButton);
        final Button cancelButton = (Button) findViewById(R.id.RegCancelButton);

        //Cancel button stops the activity

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //When the register button is clicked

        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                //User inputted text is assigned a variable
                String firstName = edFirstName.getText().toString();
                String lastName = edLastName.getText().toString();
                String regUser = edRegUser.getText().toString();
                String regPass = edRegPass.getText().toString();
                String confPass = edConfPass.getText().toString();

                //Response listener

                Response.Listener<String> listener =  new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            //Converting the string to json object

                            JSONObject jsonObject = new JSONObject(response);

                            //Getting boolean from jsonObject
                            boolean success = jsonObject.getBoolean("success");

                            if(success) {

                                //Register success

                                AlertDialog.Builder registerSuccess = new AlertDialog.Builder(RegisterActivity.this);
                                registerSuccess.setTitle("Registration Success")
                                        .setNegativeButton("Log In Now", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                finish();

                                            }
                                        })
                                        .create()
                                        .show();
                            }
                            else{

                                //Register fail
                                AlertDialog.Builder registerFail = new AlertDialog.Builder(RegisterActivity.this);
                                registerFail.setTitle("Registration Failed")
                                        .setMessage("Username is already in use.")
                                        .setNegativeButton("Try Again", null)
                                        .create()
                                        .show();
                            }
                        }
                        catch (JSONException e){
                            e.printStackTrace();
                        }

                    }
                };

                //Sends the user inputted data and inserts it into a table
                RegisterRequest registerRequest = new RegisterRequest(regUser, firstName, lastName, regPass, listener);
                RequestQueue requestQueue = Volley.newRequestQueue(RegisterActivity.this);

                //Invalid characters or password mismatch

                if(regPass.equals(confPass)){
                    String message = "";

                    String[] invalidChars = new String[] {" ", "/", "?", ".", ">", "<", ",", "'", "\"", ";", ":", "]", "}", "[","{", "=", "+", "-", "_", ")", "(", "*", "&", "^", "%", "$", "#", "@", "!"};
                    for(int i = 0; i < invalidChars.length; i++) {

                        message = message + invalidChars[i] + " ";

                    }

                    for(int i = 0; i < invalidChars.length; i++) {
                        if (regUser.indexOf(invalidChars[i]) < 0) {

                            //If no invalid characters are found, the request will be executed

                            if (i == invalidChars.length - 1) {
                                requestQueue.add(registerRequest);
                            }

                        } else {

                            //Invalid characters

                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                            builder.setTitle("Registration Failed")
                                    .setMessage("Username can't contain the following characters: " + message)
                                    .setNegativeButton("Try Again", null)
                                    .create()
                                    .show();

                            i = invalidChars.length; //stops the for loop
                        }
                    }
                }
                else{

                    //Password mismatch

                    AlertDialog.Builder passwordMismatch = new AlertDialog.Builder(RegisterActivity.this);
                    passwordMismatch.setMessage("The passwords do not match. Please Try Again.")
                            .setNegativeButton("Try Again", null)
                            .create()
                            .show();
                }
            }
        });

    }
}

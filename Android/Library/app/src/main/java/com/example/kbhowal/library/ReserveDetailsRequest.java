package com.example.kbhowal.library;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmhbh on 2/26/2018.
 */

public class ReserveDetailsRequest extends StringRequest {

    private static final String RESERVE_URL = "https://fblalibrary.000webhostapp.com/reserve_details.php";
    private Map<String, String> params;


    //Data that will be sent to the database

    public ReserveDetailsRequest(int bookId, Response.Listener<String> listener){
        super(Method.POST, RESERVE_URL, listener, null);

        params = new HashMap<>();
        params.put("bookId", String.valueOf(bookId));
    }

    //Override for getParams() method

    public Map<String, String> getParams(){

        return params;
    }
}

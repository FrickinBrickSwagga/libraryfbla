package com.example.kbhowal.library;

        import android.app.Fragment;
        import android.content.Context;
        import android.os.Bundle;
        import android.support.annotation.Nullable;
        import android.support.design.widget.AppBarLayout;
        import android.support.design.widget.CollapsingToolbarLayout;
        import android.support.design.widget.Snackbar;
        import android.support.v7.app.AppCompatActivity;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.util.DisplayMetrics;
        import android.util.Log;

        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;

        import android.view.WindowManager;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.RelativeLayout;
        import android.widget.ScrollView;
        import android.widget.Button;
        import android.widget.TextView;
        import android.support.v7.widget.Toolbar;

        import com.android.volley.AuthFailureError;
        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;

        import com.squareup.picasso.Callback;
        import com.squareup.picasso.Picasso;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.lang.reflect.Array;
        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

/**
 * Created by kbhowal on 11/10/2017.
 */

public class CatalogFragment extends Fragment {


    //Initializes variables
    Toolbar toolbarCat;
    private static final String DATA_URL = "https://fblalibrary.000webhostapp.com/book_chars.php";    //Stores the database URL in a variable  //https://fblalibrary.000webhostapp.com/  /http://10.0.2.2/proj/index.php


    List<Book> bookListCatalogDisplay;

    RecyclerView recyclerView;

    //Sets content view

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.catalog_layout, container, false);

        bookListCatalogDisplay = new ArrayList<>();

        //RecyclerView settings

        recyclerView = view.findViewById(R.id.rCatalogView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        fetchBooks();

        return view;

    }



    public void fetchBooks(){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, DATA_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {


                    //Converting the string to json array object
                    JSONArray array = new JSONArray(response);

                    //traversing through all the objects
                    for (int i = 0; i < array.length(); i++) {

                        //getting book object from json array
                        JSONObject book = array.getJSONObject(i);

                        //adding the books to book list
                        if( i == 0 || !(array.getJSONObject(i - 1).getInt("bookId") == book.getInt("bookId"))) {
                            bookListCatalogDisplay.add(new Book(
                                    book.getInt("bookId"),
                                    book.getString("uniqueId"),
                                    book.getString("bookName"),
                                    book.getString("authorName"),
                                    book.getString("bookPages"),
                                    book.getString("Language"),
                                    book.getString("cover"),
                                    book.getString("shortdesc")

                            ));

                        }

                    }

                    //creating adapter object and setting it to recyclerView
                    CatalogAdapter adapter = new CatalogAdapter(getContext(), bookListCatalogDisplay);
                    recyclerView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, null);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(stringRequest);

    }

}

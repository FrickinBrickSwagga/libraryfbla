package com.example.kbhowal.library;


import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmhbh on 12/29/2017.
 */

public class CheckOutRequest extends StringRequest {

    private static final String CHECKOUT_URL = "https://fblalibrary.000webhostapp.com/check_out.php";
    private Map<String, String> params;


    //Data that will be sent to the database

    public CheckOutRequest(int bookId, String username, String reserved, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, CHECKOUT_URL, listener, errorListener);

        params = new HashMap<>();
        params.put("bookId", String.valueOf(bookId));
        params.put("Username", username);
        params.put("Reserved", reserved);
    }

    //Override for getParams() method

    public Map<String, String> getParams(){

        return params;
    }


}


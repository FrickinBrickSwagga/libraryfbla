package com.example.kbhowal.library;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kmhbh on 2/19/2018.
 */

public class BookRequest extends StringRequest{

    private static final String GET_BOOK_URL = "https://fblalibrary.000webhostapp.com/get_book_info.php";
    private Map<String, String> params;



    //Data that will be sent to the database

    public BookRequest(String Id, Response.Listener<String> listener) {
        super(Request.Method.POST, GET_BOOK_URL, listener, null);

        params = new HashMap<>();
        params.put("Id", Id);

    }

    //Override for getParams() method

    public Map<String, String> getParams(){

        return params;
    }
}

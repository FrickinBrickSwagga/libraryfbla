<?php
require 'db/connect.php';

if(isset($_POST['Username']) && !empty($_POST['Username'])){
    $username = $_POST['Username'];
}
    
if(isset($_POST['FirstName']) && !empty($_POST['FirstName'])){
    $firstName = $_POST['FirstName'];
}
    
if(isset($_POST['LastName']) && !empty($_POST['LastName'])){
    $lastName = $_POST['LastName'];
}
    
if(isset($_POST['Password']) && !empty($_POST['Password'])){
    $password = $_POST['Password'];
}



$stmtUser = $db->prepare("SELECT `Username` FROM `User Details`");		//Prepares the statement for execution
$stmtUser->execute();		//Executes $statement
$stmtUser->bind_result($otherUsernames); 		//Binds results to local variables

$userArray = array();
$userArray["success"] = true;

while($stmtUser->fetch()){
    
    if($otherUsernames == $username){
        $userArray["success"] = false;
    }
}

if($userArray["success"]){
    
    $stmtInsert = $db->prepare("INSERT INTO `User Details`(`Username`, `FirstName`, `LastName`, `Password`) VALUES (?, ?, ?, ?)");
    $stmtInsert->bind_param("ssss", $username, $firstName, $lastName, $password);
    $stmtInsert->execute();
    
}

echo json_encode($userArray);

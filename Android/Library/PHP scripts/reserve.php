<?php

require 'db/connect.php';

if(isset($_POST['bookId']) && !empty($_POST['bookId'])){
    $bookId = $_POST['bookId'];
}
if(isset($_POST['Username']) && !empty($_POST['Username'])){
    $username = $_POST['Username'];
}

$reserveArray = array();

//Checking for available reserve slot


if($stmtSpaceCheck = $db->prepare("SELECT `Reserve1` FROM `User Details` WHERE `Username` = ?")){
    $stmtSpaceCheck->bind_param("s", $username);
    $stmtSpaceCheck->execute();
    $stmtSpaceCheck->bind_result($reserve);
    $stmtSpaceCheck->fetch();
    $stmtSpaceCheck->close();


    //If one is empty, appropriate data will be inserted
    
    if(empty($reserve)){
        
        $stmtInsertReserve = $db->prepare("UPDATE `User Details` SET `Reserve1` = ? WHERE `Username` = ?");
        $stmtInsertReserve->bind_param("ss", $bookId, $username);
        $stmtInsertReserve->execute();
        $stmtInsertReserve->close();
        
        $reserveArray['book'] = $bookId;
        $reserveArray['success'] = true;
        $reserveArray['message'] = "Reservation Placed";
    }
    else{
        
        $reserveArray['success'] = false;
        $reserveArray['message'] = "You may not reserve more than 1 book";
    }
    
    echo json_encode ($reserveArray);
    
}
<?php

require 'db/connect.php';

if(isset($_POST['Username']) && !empty($_POST['Username'])){
    $username = $_POST['Username'];
    
}

if(isset($_POST['Password']) && !empty($_POST['Password'])){
    $password = $_POST['Password'];
    
}


$stmt = $db->prepare("SELECT Username, FirstName, LastName, Password FROM `User Details` WHERE `Username` = ? AND `Password` = ?");
$stmt->bind_param("ss", $username, $password);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($username, $firstName, $lastName, $password);

$response = array();
$response["success"] = false;

while($stmt->fetch()){
    $response["success"] = true;
    $response["Username"] = $username;
    $response["FirstName"] = $firstName;
    $response["LastName"] = $lastName;
    $response["Password"] = $password;
    
}    
    echo json_encode ($response);

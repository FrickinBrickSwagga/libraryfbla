<?php

require 'db/connect.php';

error_reporting(E_ALL & ~E_NOTICE);

//Initializing required inputs and variables

if(isset($_POST['bookId']) && !empty($_POST['bookId'])){
    $bookId = $_POST['bookId'];
}
if(isset($_POST['Username']) && !empty($_POST['Username'])){
    $username = $_POST['Username'];
}
if(isset($_POST['Reserved']) && !empty($_POST['Reserved'])){
    $reserving = $_POST['Reserved'];
}




$blank = "";

$dateCheckedOut = date("Y-m-d");
$dateDue = date('Y-m-d', strtotime($dateCheckedOut. ' + 7 days'));
$checkedOutTrue = 1;
$copies = 0;

//Checks if copies of selected book are avaiblable 

$stmtCopyCheck = $db->prepare("SELECT `bookId`, `checkedOut`, `uniqueId` FROM `Books` WHERE `bookId` = ? AND `checkedOut` = 0");
$stmtCopyCheck->bind_param("s", $bookId);
$stmtCopyCheck->execute();
$stmtCopyCheck->bind_result($bookIds, $isCheckedOut, $uniqueId);

while($stmtCopyCheck->fetch()){
    $array = array();
    $array['isCheckedOut'] = $isCheckedOut;
    
    $copies++;
    
}

$stmtCopyCheck->close();

$checkOutArray = array();
$checkOutArray['success'] = true;
$checkOutArray['reservable'] = false;

//If no copies are avaiblable, book will not be checked out.

if($copies <= 0){
    
    $checkOutArray['success'] = false;
    $checkOutArray['message'] = "There are no more copies left of this book. You may reserve a copy of this book for when it becomes avaiblable.";   
}

//Checks if book is reserved

else if($stmtSpaceCheck = $db->prepare("SELECT `Reserve1` FROM `User Details`")){
    $stmtSpaceCheck->execute();
    $stmtSpaceCheck->bind_result($reserved);
    while($stmtSpaceCheck->fetch()){
        
        if($reserved == $bookId){
            
            $checkOutArray['success'] = false;
            $checkOutArray['message'] = "This book is currently reserved by another user and cannot be checked out.";
            
        }
        
    }
    $stmtSpaceCheck->close();
    
    $stmtUserCheck = $db->prepare("SELECT `Reserve1` FROM `User Details` WHERE `Username` = ?");
    $stmtUserCheck->bind_param("s", $username);
    $stmtUserCheck->execute();
    $stmtUserCheck->bind_result($userReservation);
    $stmtUserCheck->fetch();
    $stmtUserCheck->close();


    if($userReservation == $bookId){
        
        $checkOutArray['success'] = true;
        
    }
}

//If copies are avaiblalable, it will look for avaibale CheckOut slots

if($checkOutArray['success']){
    if($stmtSpaceCheck = $db->prepare("SELECT `CheckOut1` FROM `User Details` WHERE `Username` = ?")){
        $stmtSpaceCheck->bind_param("s", $username);
        $stmtSpaceCheck->execute();
        $stmtSpaceCheck->bind_result($checkOut);
        $stmtSpaceCheck->fetch();
        $stmtSpaceCheck->close();


        //If one is empty, appropriate data will be inserted
        
        if(empty($checkOut)){
            
            $stmtInsertCheckOut = $db->prepare("UPDATE `User Details` SET `CheckOut1` = ?, `coDate1` = ?, dueDate1 = ? WHERE `Username` = ?");
            $stmtInsertCheckOut->bind_param("ssss", $uniqueId, $dateCheckedOut, $dateDue, $username);
            $stmtInsertCheckOut->execute();
            $stmtInsertCheckOut->close();
            
            $stmtUpdateCopies = $db->prepare("UPDATE `Books` SET `checkedOut` = ? WHERE `uniqueId` = ?");
            $stmtUpdateCopies->bind_param("is", $checkedOutTrue, $uniqueId);
            $stmtUpdateCopies->execute();
            $stmtUpdateCopies->close();
            
            
            
            $checkOutArray['success'] = true;
        }
        
        //Repeats everything by checking next slot
        
        else if($stmtSpaceCheck = $db->prepare("SELECT `CheckOut2` FROM `User Details` WHERE `Username` = ?")){
        
            $stmtSpaceCheck->bind_param("s", $username);
            $stmtSpaceCheck->execute();
            $stmtSpaceCheck->bind_result($checkOut);
            $stmtSpaceCheck->fetch();
            $stmtSpaceCheck->close();
        
            if(empty($checkOut)){
                
                $stmtInsertCheckOut = $db->prepare("UPDATE `User Details` SET `CheckOut2` = ?, `coDate2` = ?, dueDate2 = ? WHERE `Username` = ?");
                $stmtInsertCheckOut->bind_param("ssss", $uniqueId, $dateCheckedOut, $dateDue, $username);
                $stmtInsertCheckOut->execute();
                $stmtInsertCheckOut->close();
                
                $stmtUpdateCopies = $db->prepare("UPDATE `Books` SET `checkedOut` = ? WHERE `uniqueId` = ?");
                $stmtUpdateCopies->bind_param("is", $checkedOutTrue, $uniqueId);
                $stmtUpdateCopies->execute();
                $stmtUpdateCopies->close();
                
                
                
                $checkOutArray['success'] = true;
                }
        
                else if($stmtSpaceCheck = $db->prepare("SELECT `CheckOut3` FROM `User Details` WHERE `Username` = ?")){
    
                    $stmtSpaceCheck->bind_param("s", $username);
                    $stmtSpaceCheck->execute();
                    $stmtSpaceCheck->bind_result($checkOut);
                    $stmtSpaceCheck->fetch();
                    $stmtSpaceCheck->close();
    
                    if(empty($checkOut)){
                        
                        $stmtInsertCheckOut = $db->prepare("UPDATE `User Details` SET `CheckOut3` = ?, `coDate3` = ?, dueDate3 = ? WHERE `Username` = ?");
                        $stmtInsertCheckOut->bind_param("ssss", $uniqueId, $dateCheckedOut, $dateDue, $username);
                        $stmtInsertCheckOut->execute();
                        $stmtInsertCheckOut->close();
                        
                        $stmtUpdateCopies = $db->prepare("UPDATE `Books` SET `checkedOut` = ? WHERE `uniqueId` = ?");
                        $stmtUpdateCopies->bind_param("is", $checkedOutTrue, $uniqueId);
                        $stmtUpdateCopies->execute();
                        $stmtUpdateCopies->close();
                        
                        $checkOutArray['success'] = true;
        
                    }
                    
                    else if($stmtSpaceCheck = $db->prepare("SELECT `CheckOut4` FROM `User Details` WHERE `Username` = ?")){
        
                        $stmtSpaceCheck->bind_param("s", $username);
                        $stmtSpaceCheck->execute();
                        $stmtSpaceCheck->bind_result($checkOut);
                        $stmtSpaceCheck->fetch();
                        $stmtSpaceCheck->close();
                        
                        if(empty($checkOut)){
                            
                            $stmtInsertCheckOut = $db->prepare("UPDATE `User Details` SET `CheckOut4` = ?, `coDate4` = ?, dueDate4 = ? WHERE `Username` = ?");
                            $stmtInsertCheckOut->bind_param("ssss", $uniqueId, $dateCheckedOut, $dateDue, $username);
                            $stmtInsertCheckOut->execute();
                            $stmtInsertCheckOut->close();
                            
                            $stmtUpdateCopies = $db->prepare("UPDATE `Books` SET `checkedOut` = ? WHERE `uniqueId` = ?");
                            $stmtUpdateCopies->bind_param("is", $checkedOutTrue, $uniqueId);
                            $stmtUpdateCopies->execute();
                            $stmtUpdateCopies->close();
                            
                            $checkOutArray['success'] = true;
                        
                        }
                        else if($stmtSpaceCheck = $db->prepare("SELECT `CheckOut5` FROM `User Details` WHERE `Username` = ?")){
        
                            $stmtSpaceCheck->bind_param("s", $username);
                            $stmtSpaceCheck->execute();
                            $stmtSpaceCheck->bind_result($checkOut);
                            $stmtSpaceCheck->fetch();
                            $stmtSpaceCheck->close();
                            
                            if(empty($checkOut)){
                                
                                $stmtInsertCheckOut = $db->prepare("UPDATE `User Details` SET `CheckOut5` = ?, `coDate5` = ?, dueDate5 = ? WHERE `Username` = ?");
                                $stmtInsertCheckOut->bind_param("ssss", $uniqueId, $dateCheckedOut, $dateDue, $username);
                                $stmtInsertCheckOut->execute();
                                $stmtInsertCheckOut->close();
                                
                                $stmtUpdateCopies = $db->prepare("UPDATE `Books` SET `checkedOut` = ? WHERE `uniqueId` = ?");
                                $stmtUpdateCopies->bind_param("is", $checkedOutTrue, $uniqueId);
                                $stmtUpdateCopies->execute();
                                $stmtUpdateCopies->close();
                                
                                $checkOutArray['success'] = true;
                            
                            }
                            
                            
                            
                            //If no CheckOut slots are empty, book will not be checked out
                            else{
                                
                                $checkOutArray['success'] = false;
                                $checkOutArray['message'] = "You may not check out more than 5 books.";
                                
                            }
                        }
                    }
                    
                }

        }
        
    }
    
}
    
    

if($checkOutArray['success']){
    
    $checkOutArray['message'] = "Successfully Checked Out.";
    
}


if($reserving == "Reserved"){
                
    $stmtUpdateReservations = $db->prepare("UPDATE `User Details` SET `Reserve1` = ? WHERE `Username` = ?");
    $stmtUpdateReservations->bind_param("ss", $blank, $username);
    $stmtUpdateReservations->execute();
    
}


echo json_encode($checkOutArray);

<?php

require 'db/connect.php';


if(isset($_POST['bookId']) && !empty($_POST['bookId'])){
    $bookId = $_POST['bookId'];
}



$reserve = array();

$reserve['available'] = false;
$reserve['message'] = "Not Available yet";

$stmtAvailability = $db->prepare("SELECT checkedOut FROM `Books` WHERE `bookId` = ?");
$stmtAvailability->bind_param("s", $bookId);
$stmtAvailability->execute();
$stmtAvailability->bind_result($isCheckedOut);


while($stmtAvailability->fetch()){
    
    if($isCheckedOut == 0){
        
        $reserve['available'] = true;  
        $reserve['message'] = "Ready for Checkout";
        
    }
    
    
}

echo json_encode ($reserve);
<?php

	require 'db/connect.php'; //runs code in connect.php
	
	
	$stmt = $db->prepare("SELECT bookId, uniqueId, bookName, authorName, bookPages, Language, cover, shortdesc FROM Books;");		//Prepares the statement for execution
	$stmt->execute();		//Executes $statement
	$stmt->bind_result($bookId, $uniqueId, $bookName, $authorName, $bookPages, $Language, $cover, $shortdesc); 		//Binds results to local variables
	

	
	
    
    $books = array(); 		//Initilizes $books as an array

	while($stmt->fetch()){		//Fetches data from database
		
		$temp = array();
		$temp['bookId'] = $bookId;
		$temp['uniqueId'] = $uniqueId;
		$temp['bookName'] = $bookName;
		$temp['authorName'] = $authorName;	//All data from database is stored in a temporary array
		$temp['bookPages'] = $bookPages;		
		$temp['Language'] = $Language;
	    $temp['cover'] = $cover;
	    $temp['shortdesc'] = $shortdesc;

		array_push($books, $temp);		//Data from temporary array is pushed to $books array
	}
    
	echo json_encode($books);		//Encodes the array in json format
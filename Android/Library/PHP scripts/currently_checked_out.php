<?php

require 'db/connect.php';

error_reporting(E_ALL & ~E_NOTICE);


if(isset($_POST['Username']) && !empty($_POST['Username'])){
    $username = $_POST['Username'];
}

$stmtGetBook = $db->prepare("SELECT CheckOut1, CheckOut2, CheckOut3, CheckOut4, CheckOut5, coDate1, coDate2, coDate3, coDate4, coDate5, dueDate1, dueDate2, dueDate3, dueDate4, dueDate5, Reserve1, FirstName, LastName FROM `User Details` WHERE `Username` = ?");

$stmtGetBook->bind_param("s", $username);

$stmtGetBook->execute();		//Executes $statement

$stmtGetBook->bind_result($checkOut1, $checkOut2, $checkOut3, $checkOut4, $checkOut5, $coDate1, $coDate2, $coDate3, $coDate4, $coDate5, $dueDate1, $dueDate2, $dueDate3, $dueDate4, $dueDate5, $reserve1, $firstName, $lastName);		//Binds results to local variables

$books = array(); 		//Initilizes $books as an array




while($stmtGetBook->fetch()){		//Fetches data from database
	
	$books['CheckOut1'] = $checkOut1;
	$books['CheckOut2'] = $checkOut2;
	$books['CheckOut3'] = $checkOut3;
	$books['CheckOut4'] = $checkOut4;	//All data from database is stored in an array
	$books['CheckOut5'] = $checkOut5;
    $books['coDate1'] = $coDate1;
    $books['dueDate1'] = $dueDate1;
    $books['coDate2'] = $coDate2;
    $books['dueDate2'] = $dueDate2;
    $books['coDate3'] = $coDate3;
    $books['dueDate3'] = $dueDate3;
    $books['coDate4'] = $coDate4;
    $books['dueDate4'] = $dueDate4;
    $books['coDate5'] = $coDate5;
    $books['dueDate5'] = $dueDate5;
    $books['reserve1'] = $reserve1;
	$books['firstName'] = $firstName;
	$books['lastName'] = $lastName;
	
	
	
}

echo json_encode($books);		//Encodes the array in json format

<?php

require 'db/connect.php';

if(isset($_POST['Id']) && !empty($_POST['Id'])){
    $Id = $_POST['Id'];
    trim($Id);
}

$stmtGetBook = $db->prepare("SELECT bookId, uniqueId, bookName, authorName, bookPages, Language, cover, shortdesc FROM `Books` WHERE `uniqueId` = ? OR `bookId` = ?");

$stmtGetBook->bind_param("ss", $Id, $Id);

$stmtGetBook->execute();		//Executes $statement

$stmtGetBook->bind_result($bookId, $uniqueId, $bookName, $authorName, $bookPages, $Language, $cover, $shortdesc); 		//Binds results to local variables

$books = array(); 		//Initilizes $books as an array

	while($stmtGetBook->fetch()){		//Fetches data from database
		
		$books['bookId'] = $bookId;
		$books['uniqueId'] = $uniqueId;
		$books['bookName'] = $bookName;
		$books['authorName'] = $authorName;	//All data from database is stored in a temporary array
		$books['bookPages'] = $bookPages;		
		$books['Language'] = $Language;
	    $books['cover'] = $cover;
	    $books['shortdesc'] = $shortdesc;
		

	}

echo json_encode($books);		//Encodes the array in json format
<?php

require 'db/connect.php';

$checkedOutFalse = 0;
$blank = "";

if(isset($_POST['uniqueId']) && !empty($_POST['uniqueId'])){
    $uniqueId = $_POST['uniqueId'];
}

if(isset($_POST['Username']) && !empty($_POST['Username'])){
    $username = $_POST['Username'];
}

$stmtCheckOut = $db->prepare("SELECT CheckOut1, CheckOut2, CheckOut3, CheckOut4, CheckOut5, coDate1, coDate2, coDate3, coDate4, coDate5, dueDate1, dueDate2, dueDate3, dueDate4, dueDate5 FROM `User Details` WHERE `Username` = ?");
$stmtCheckOut->bind_param("s", $username);
$stmtCheckOut->execute();		//Executes $statement
$stmtCheckOut->bind_result($checkOut1, $checkOut2, $checkOut3, $checkOut4, $checkOut5, $coDate1, $coDate2, $coDate3, $coDate4, $coDate5, $dueDate1, $dueDate2, $dueDate3, $dueDate4, $dueDate5); //binds results

$books = array();

while($stmtCheckOut->fetch()){
    
    $books['CheckOut1'] = $checkOut1;
	$books['CheckOut2'] = $checkOut2;
	$books['CheckOut3'] = $checkOut3;
	$books['CheckOut4'] = $checkOut4;	//All data from database is stored in an array
	$books['CheckOut5'] = $checkOut5;
	$books['coDate1'] = $coDate1;
	$books['coDate2'] = $coDate2;
	$books['coDate3'] = $coDate3;
	$books['coDate4'] = $coDate4;
	$books['coDate5'] = $coDate5;
	$books['dueDate1'] = $dueDate1;
	$books['dueDate2'] = $dueDate2;
	$books['dueDate3'] = $dueDate3;
	$books['dueDate4'] = $dueDate4;
	$books['dueDate5'] = $dueDate5;
}


$stmtCheckOut->close();

$checkOutSlot = array_search($uniqueId, $books);

$coDateSlot = "";
$dueDateSlot = "";

switch ($checkOutSlot) {
    case "CheckOut1":
        $coDateSlot = "coDate1";
        $dueDateSlot = "dueDate1";
        break;
    case "CheckOut2":
        $coDateSlot = "coDate2";
        $dueDateSlot = "dueDate2";
        break;
    case "CheckOut3":
        $coDateSlot = "coDate3";
        $dueDateSlot = "dueDate3";
        break;
    case "CheckOut4":
        $coDateSlot = "coDate4";
        $dueDateSlot = "dueDate4";
        break;
    case "CheckOut5":
        $coDateSlot = "coDate5";
        $dueDateSlot = "dueDate5";
        break;
}

echo $dueDateSlot;
echo $coDateSlot;

$stmtReturn = $db->prepare("UPDATE `User Details` SET $checkOutSlot = ?, $coDateSlot = ?, $dueDateSlot = ? WHERE `Username` = ?");
$stmtReturn->bind_param("ssss", $blank, $blank, $blank, $username);
$stmtReturn->execute();		//Executes $statement
$stmtReturn->close();

$stmtCheckIn = $db->prepare("UPDATE `Books` SET `checkedOut` = ? WHERE `uniqueId` = ?");
$stmtCheckIn->bind_param("is", $checkedOutFalse, $uniqueId);
$stmtCheckIn->execute();

echo json_encode($books);		//Encodes the array in json format